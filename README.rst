OnBuy
*****

Django OnBuy
https://www.kbsoftware.co.uk/docs/app-onbuy.html

Install
=======

Virtual Environment
-------------------

::

  python3 -m venv venv-onbuy
  # or
  virtualenv --python=python3 venv-onbuy

  source venv-onbuy/bin/activate
  pip install -r requirements/local.txt

Testing
=======

::

  find . -name '*.pyc' -delete
  pytest -x

Usage
=====

::

  ./init_dev.sh

Release
=======

https://www.kbsoftware.co.uk/docs/
