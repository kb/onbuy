# -*- encoding: utf-8 -*-
import pytest

from django.core.management import call_command


@pytest.mark.django_db
def test_demo_data():
    call_command("demo-data-onbuy")
