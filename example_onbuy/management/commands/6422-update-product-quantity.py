# -*- encoding: utf-8 -*-
from decimal import Decimal
from django.core.management.base import BaseCommand
from rich.pretty import pprint

from onbuy.service import OnBuyProduct, update_products


class Command(BaseCommand):

    help = "OnBuy - update product (testing) #6422"

    def handle(self, *args, **options):
        self.stdout.write(f"{self.help}")
        products = [
            OnBuyProduct(sku="AK113278", price=Decimal("349.00"), quantity=0),
        ]
        update_products(products)
        self.stdout.write(f"{self.help} - Complete")
