# -*- encoding: utf-8 -*-
import attr
import json
import requests

from decimal import Decimal
from django.conf import settings
from http import HTTPStatus

from .models import OnBuyError


@attr.s
class OnBuyProduct:
    sku = attr.ib()
    price = attr.ib()
    quantity = attr.ib()


def _products_to_listings(products):
    result = []
    for product in products:
        result.append(
            {
                "sku": product.sku,
                "price": str(product.price),
                "stock": product.quantity,
            }
        )
    return result


def _url_onbuy_listings(offset, limit):
    """GET - Product Listings: Browse."""
    return f"{settings.ONBUY_API_URL}listings?site_id={settings.ONBUY_SITE_ID}&limit={limit}&offset={offset}"


def _url_onbuy_listings_by_sku():
    """PUT - Product Listings: Update by SKU"""
    return f"{settings.ONBUY_API_URL}listings/by-sku"


def _access_token():
    result = None
    url = f"{settings.ONBUY_API_URL}auth/request-token"
    payload = {
        "secret_key": settings.ONBUY_SECRET_KEY,
        "consumer_key": settings.ONBUY_CONSUMER_KEY,
    }
    files = []
    headers = {}
    response = requests.request(
        "POST", url, headers=headers, data=payload, files=files
    )
    if response.status_code == HTTPStatus.OK:
        data = response.json()
        result = data["access_token"]
    else:
        raise OnBuyError(
            f"Cannot retrieve access token.  Status code {response.status_code}"
        )
    return result


def product_skus():
    result = []
    headers = {"Authorization": _access_token()}
    limit = 50
    offset = 0
    while True:
        url = _url_onbuy_listings(offset, limit)
        response = requests.request("GET", url, headers=headers, data={})
        if response.status_code == HTTPStatus.OK:
            data = response.json()
            products = data["results"]
            for product in products:
                result.append(
                    OnBuyProduct(
                        sku=product["sku"],
                        price=Decimal(product["price"]),
                        quantity=product["stock"],
                    )
                )
            if len(products) < limit:
                break
            offset = offset + limit
        else:
            raise OnBuyError(
                f"Cannot retrieve products.  Status code {response.status_code}"
            )
    return result


def update_products(products):
    count = 0
    url = _url_onbuy_listings_by_sku()
    headers = {
        "Authorization": _access_token(),
        "Content-Type": "application/json",
    }
    payload = json.dumps(
        {
            "site_id": settings.ONBUY_SITE_ID,
            "listings": _products_to_listings(products),
        }
    )
    response = requests.request("PUT", url, headers=headers, data=payload)
    if response.status_code == HTTPStatus.OK:
        pass
    else:
        raise OnBuyError(
            f"Cannot update products.  Status code {response.status_code}"
        )
    return len(products)
