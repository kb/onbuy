# -*- encoding: utf-8 -*-
from django.core.management.base import BaseCommand
from rich.pretty import pprint

from onbuy.service import product_skus


class Command(BaseCommand):

    help = "OnBuy Product SKUs"

    def handle(self, *args, **options):
        self.stdout.write(f"{self.help}")
        skus = product_skus()
        pprint(skus, expand_all=True)
        count = len(set([x.sku for x in skus]))
        self.stdout.write(f"{self.help} - Complete.  Found {count} products.")
