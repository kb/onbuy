# -*- encoding: utf-8 -*-
import pytest
import responses

from decimal import Decimal
from http import HTTPStatus

# from rich.pretty import pprint

from onbuy.service import (
    _products_to_listings,
    OnBuyProduct,
    product_skus,
    update_products,
)


@responses.activate
def test_product_skus():
    responses.add(
        responses.POST,
        "https://api.onbuy.com/v2/auth/request-token",
        json={"access_token": "AT55"},
        status=HTTPStatus.OK,
    )
    responses.add(
        responses.GET,
        "https://api.onbuy.com/v2/listings",
        json={"results": [{"sku": "A1", "price": "12.34", "stock": 3}]},
        status=HTTPStatus.OK,
    )
    assert [
        OnBuyProduct(sku="A1", price=Decimal("12.34"), quantity=3)
    ] == product_skus()


def test_products_to_listings():
    products = [
        OnBuyProduct(sku="SKU-ABC-1", price=Decimal("17.66"), quantity=1),
        OnBuyProduct(sku="SKU-ABC-2", price=Decimal("16.66"), quantity=2),
        OnBuyProduct(sku="SKU-ABC-3", price=Decimal("15.66"), quantity=33),
        OnBuyProduct(sku="SKU-ABC-4", price=Decimal("14.66"), quantity=-1),
    ]
    result = _products_to_listings(products)
    assert [
        {
            "sku": "SKU-ABC-1",
            "price": "17.66",
            "stock": 1,
        },
        {
            "sku": "SKU-ABC-2",
            "price": "16.66",
            "stock": 2,
        },
        {
            "sku": "SKU-ABC-3",
            "price": "15.66",
            "stock": 33,
        },
        {
            "sku": "SKU-ABC-4",
            "price": "14.66",
            "stock": -1,
        },
    ] == result


@responses.activate
def test_update_products():
    responses.add(
        responses.POST,
        "https://api.onbuy.com/v2/auth/request-token",
        json={"access_token": "AT55"},
        status=HTTPStatus.OK,
    )
    responses.add(
        responses.PUT,
        "https://api.onbuy.com/v2/listings/by-sku",
        json={},
        status=HTTPStatus.OK,
    )
    products = [
        OnBuyProduct(sku="A1", price="12.34", quantity=5),
        OnBuyProduct(sku="B2", price="23.45", quantity=33),
    ]
    assert 2 == update_products(products)
