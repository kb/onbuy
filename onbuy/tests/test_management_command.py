# -*- encoding: utf-8 -*-
import pytest
import responses

from django.core.management import call_command
from http import HTTPStatus


@pytest.mark.django_db
@responses.activate
def test_onbuy_product_skus():
    responses.add(
        responses.POST,
        "https://api.onbuy.com/v2/auth/request-token",
        json={"access_token": "AT55"},
        status=HTTPStatus.OK,
    )
    responses.add(
        responses.GET,
        "https://api.onbuy.com/v2/listings",
        json={"results": []},
        status=HTTPStatus.OK,
    )
    call_command("onbuy-product-skus")
